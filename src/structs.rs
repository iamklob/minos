use std::ops::Index;

#[derive(Debug)]
pub struct Grid {
    xsize: usize,
    ysize: usize,
    pub cells: Vec<Vec<Cell>>
}

impl Grid {
    pub fn new (xsize: usize, ysize: usize) -> Grid {
        let mut grid = Grid { xsize, ysize, cells: Vec::new() };

        for row in 0..xsize {
            let mut row_vec = Vec::new();
            for column in 0..ysize {
                row_vec.push(Cell { row, column })
            }
            grid.cells.push(row_vec);
        }

        grid
    }
}

impl Index<usize> for Grid {
    type Output = Vec<Cell>;

    fn index(&self, index: usize) -> &Vec<Cell> {
        &self.cells[index]
    }
}

#[derive(Debug)]
pub struct Cell {
    row: usize,
    column: usize,
}
