use minos::structs::Grid;
fn main() {
    let g = Grid::new(10, 10);

    println!("Grid: {:#?}", g);

    let cell = &g[0][2];

    println!("Cell: {:?}", cell);
}
